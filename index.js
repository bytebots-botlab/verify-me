const { Client, Collection, MessageEmbed } = require("discord.js");
const { config } = require("dotenv");
const glob = require("glob");

client = new Client({
    disableEveryone: true
});

client.commands = new Collection();

config({
    path: __dirname + "/.env"
});

client.login(process.env.TOKEN)

client.on('ready', () => {
    console.log("Time to verify people, aye?")
    setInterval(() => {
        client.user.setActivity(`verifications | %verifyme`, { type: 'WATCHING' });
    }, 10000);

    const embed = new MessageEmbed()
        .setTitle("Bot has been rebooted!")
        .setDescription("Everything is now fully operational again. If one of the users were in middle of verification they might need to use command `%verifyme` again.")
        .setTimestamp()
        .setColor("BLACK")
        .setThumbnail(client.user.displayAvatarURL())
        .addFields(
            {name: "Server count", value: `\`${client.guilds.cache.size}\``, inline: true},
            {name: "Servers total user count", value: `\`${client.users.cache.size}\``, inline: true},
        );
    const verificationGuild = client.guilds.cache.get('764248014033780817')
    verificationGuild.channels.cache.get('768027252943880203').send(embed);
    verificationGuild.channels.cache.get('768416713187917824').send(embed);
});

// COMMAND HANDLER
const commandFiles = glob.sync('./src/commands/**/*.js');
for (const file of commandFiles) {
    const command = require(file);
    client.commands.set(command.name, command);
};

// EVENT HANDLER
const eventFiles = glob.sync('./src/events/**/*.js');
for (const file of eventFiles) {
    const event = require(file);
    const eventName = /\/events.(.*).js/.exec(file)[1];
    client.on(eventName, event.bind(null, client))

};

//ERROR LOGGING
/*process.on("unhandledRejection", error => {
    const supportserver = client.guilds.cache.get('764248014033780817')
    const logchannel = supportserver.channels.cache.get('765770560759791667')

    const URembed = new MessageEmbed()
        .setTitle("Unhandled promise rejection")
        .setColor("RED")
        .setTimestamp()
        .addFields(
            {name: "Name", value: error.name, inline: true},
            {name: "Path", value: error.path, inline: true},
            {name: "Method", value: error.method, inline:true},
            {name: "Code", value: error.code, inline: true},
            {name: "Error message", value: error.message}
        )

    logchannel.send(URembed);
})*/