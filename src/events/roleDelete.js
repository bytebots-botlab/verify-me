const { MessageEmbed } = require('discord.js');
const fs = require('fs');

module.exports = async (client, role) => {
    let fromGuildCh = JSON.parse(fs.readFileSync("./src/data/guilds.json", "utf8"))
    let UserGuildLog = role.guild.channels.cache.get(fromGuildCh[role.guild.id].log)
    let fromGuildRoles = JSON.parse(fs.readFileSync("./src/data/guildroles.json", "utf8"))

    if(role.id === fromGuildRoles[role.guild.id].role){
        const embed = new MessageEmbed()
            .setTitle("<a:alert:764872264197210172> Critical Warning")
            .setDescription("Your servers Verified role was just **DELETED!!**, make sure to assign new verified role **__ASAP__**")
            .setColor("RED")
            .addField("How to add new verified role?", ":one: Create new role.\n\n:two: Register it to bot by using command\n`%settings verifyrole <roleID/mention role>`");

        UserGuildLog.send("@everyone :arrow_down:")
        UserGuildLog.send(embed);
    }
}