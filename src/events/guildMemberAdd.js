const { MessageEmbed } = require('discord.js');
const fs = require('fs');

module.exports = async (client, member) => {

    let verifieduser = JSON.parse(fs.readFileSync("./src/data/verifieduser.json", "utf8"));
    let guildsetup = JSON.parse(fs.readFileSync("./src/data/guilds.json", "utf8"));
    let fromGuildCh = JSON.parse(fs.readFileSync("./src/data/guilds.json", "utf8"))
    let fromGuildRoles = JSON.parse(fs.readFileSync("./src/data/guildroles.json", "utf8"))
    
    if(guildsetup[member.guild.id]){
        if(verifieduser[member.user.id]){
            let UserGuildRole = member.guild.roles.cache.get(fromGuildRoles[member.guild.id].role)
            let UserGuildLog = member.guild.channels.cache.get(fromGuildCh[member.guild.id].log)
            member.roles.add(UserGuildRole);
            const embed = new MessageEmbed()
                .setTitle("Verified member has arrived <:verifyme:768390443696128001>")
                .setTimestamp()
                .setFooter(`Verified in ${verifieduser[member.user.id].verifiedGuild}`)
                .setThumbnail(member.user.displayAvatarURL())
                .setDescription(`${member.user.tag} has been verified by **Verify Me!** previously and for this reason does not need to verify again.\n\n<a:approved:765040824941281311> Verified role applied to user.`)
                .addFields(
                    {name: "Verified by", value: verifieduser[member.user.id].verifiedBy}
                );
            UserGuildLog.send(embed);
        }
    }
}