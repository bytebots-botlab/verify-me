const {MessageEmbed, Guild} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "approve",
    category: "manage",
    description: "Verify member",
    run: async (client, message, args) => {
        message.delete()

        if(!message.member.roles.cache.has('767744783971778591')) return message.reply("You don't have enough power to use `%approve` command.").then(m => m.delete({timeout: 5000}))

        const verificationGuild = client.guilds.cache.get('764248014033780817')
        let fromGuild = JSON.parse(fs.readFileSync("./src/data/verification-guild.json", "utf8"))
        let fromGuildCh = JSON.parse(fs.readFileSync("./src/data/guilds.json", "utf8"))
        let fromGuildRoles = JSON.parse(fs.readFileSync("./src/data/guildroles.json", "utf8"))
        if(fromGuild[args[0]]){
            let UserGuild = client.guilds.cache.get(fromGuild[args[0]].guildID)
            let User = UserGuild.members.cache.get(args[0])
            let UserGuildLog = UserGuild.channels.cache.get(fromGuildCh[UserGuild.id].log)
            let UserGuildRole = UserGuild.roles.cache.get(fromGuildRoles[UserGuild.id].role)
            let verifieduser = JSON.parse(fs.readFileSync("./src/data/verifieduser.json", "utf8"));

            if(verifieduser[args[0]]){ const alreadyverified = new MessageEmbed().setColor("#808080").setDescription(`${User.user.tag} has been **verified already** by ${verifieduser[args[0]].verifiedBy}`); return message.channel.send(alreadyverified)}

            const verifiedDM = new MessageEmbed()
                .setTitle("You have been verified <:verifyme:768390443696128001>")
                .setColor("#357A31")
                .setThumbnail(UserGuild.iconURL())
                .setFooter(message.author.tag, message.author.displayAvatarURL())
                .setTimestamp()
                .setDescription(`:tada: Congratulations ${User.user.tag}, you have been verified now in **${UserGuild.name}**`)
                .addFields(
                    {name: "Verified by", value: message.author.tag}
                );

            const logInternal = new MessageEmbed()
                .setTitle("Verification approved <:verifyme:768390443696128001>")
                .setColor("#357A31")
                .setTimestamp()
                .setThumbnail(User.user.displayAvatarURL())
                .setFooter(message.author.tag, message.author.displayAvatarURL())
                .addFields(
                    {name: "Verified user", value: `${User.user.tag} (${User.id})`, inline:true},
                    {name: "Verified by", value: message.author.tag, inline:true},
                    {name: "Target server", value: UserGuild.name}
                );

            const logExternal = new MessageEmbed()
                .setTitle("New user verified <:verifyme:768390443696128001>")
                .setColor("#357A31")
                .setTimestamp()
                .setThumbnail(User.user.displayAvatarURL())
                .setFooter(message.author.tag, message.author.displayAvatarURL())
                .setDescription("Verified role has been applied to user.")
                .addFields(
                    {name: "Verified user", value: `${User.user.tag} (${User.id})`, inline:true},
                    {name: "Verified by", value: message.author.tag, inline:true}
                );

            UserGuildLog.send(logExternal);
            User.send(verifiedDM);
            verificationGuild.channels.cache.get('768027252943880203').send(logInternal);

            User.roles.add(UserGuildRole);

            verifieduser[User.user.id] = {
                verifiedGuild: UserGuild.name,
                verifiedGuildID: UserGuild.id,
                verifiedBy: message.author.tag
            };
            fs.writeFile("./src/data/verifieduser.json", JSON.stringify(verifieduser), (err) => {
                if (err) console.log(err)
            });
        }
    }
}