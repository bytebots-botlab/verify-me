const {MessageEmbed} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "remove",
    category: "manage",
    description: "Remove verification",
    run: async (client, message, args) => {
        message.delete()

        //          v ALL REASONS HARD CODED CUZ I AM THE BEST v

        var RemReason = ['Underage', 'Falsification']

        //          ^ ABOVE ALL REASONS! ^

        if(!message.member.roles.cache.has('768416208570548238')) return message.reply("You don't have enough power to use `%remove` command.").then(m => m.delete({timeout: 5000}))

        if(args[0] === 'reason' || args[0] === 'reasons' || args[0] === 'idlist'){

            message.reply("**All valid removal reasons** <a:denied:765040892482158593>")
            for (var i  = 0; i < RemReason.length; i++){
                message.channel.send((i + 1) + " - " + RemReason[i])
            }
            return;
        };

        if(!args[1]) return message.reply("You need to provide user's id and reason for the denial, usage `%remove <userID> <reasonID>`\n\nAll reasons `%remove reason`").then(m => m.delete({timeout: 8000}))

        const verificationGuild = client.guilds.cache.get('764248014033780817')
        let fromGuild = JSON.parse(fs.readFileSync("./src/data/verification-guild.json", "utf8"))
        let fromGuildCh = JSON.parse(fs.readFileSync("./src/data/guilds.json", "utf8"))
        let fromGuildRoles = JSON.parse(fs.readFileSync("./src/data/guildroles.json", "utf8"))
        if(fromGuild[args[0]]){
            let UserGuild = client.guilds.cache.get(fromGuild[args[0]].guildID)
            let User = UserGuild.members.cache.get(args[0])
            let UserGuildLog = UserGuild.channels.cache.get(fromGuildCh[UserGuild.id].log)
            let verifieduser = JSON.parse(fs.readFileSync("./src/data/verifieduser.json", "utf8"));

            if(!verifieduser[args[0]]){ const alreadyverified = new MessageEmbed().setColor("#808080").setDescription(`${User.user.tag} has not been verified yet by ${verifieduser[args[0]].verifiedBy}`); return message.channel.send(alreadyverified)}

            if(args[1] > RemReason.length) return message.reply("You provided invalid reason ID, see the list using `%deny reasons`").then(m => m.delete({timeout: 5000}))

            const verifiedDM = new MessageEmbed()
                .setTitle("Your verification has been removed <a:denied:765040892482158593>")
                .setColor("#BD0014")
                .setThumbnail(UserGuild.iconURL())
                .setFooter(message.author.tag, message.author.displayAvatarURL())
                .setTimestamp()
                .setDescription(`Hello there ${User.user.tag}, your verification has been removed by our Verification Team Management.`)
                .addFields(
                    {name: "Removed by", value: message.author.tag},
                    {name: "Reason", value: RemReason[args[1] - 1]}
                );

            const logInternal = new MessageEmbed()
                .setTitle("Verification removed")
                .setColor("#BD0014")
                .setTimestamp()
                .setThumbnail(User.user.displayAvatarURL())
                .setFooter(message.author.tag, message.author.displayAvatarURL())
                .addFields(
                    {name: "User", value: `${User.user.tag} (${User.id})`, inline:true},
                    {name: "Removed by", value: message.author.tag, inline:true},
                    {name: "Reason", value: RemReason[args[1] - 1]}
                );

            const logExternal = new MessageEmbed()
                .setTitle("Your user's verification has been removed")
                .setColor("#BD0014")
                .setTimestamp()
                .setThumbnail(User.user.displayAvatarURL())
                .setFooter(message.author.tag, message.author.displayAvatarURL())
                .setDescription("User has been informed about their verification removal, make sure that the verified role is removed.")
                .addFields(
                    {name: "User", value: `${User.user.tag} (${User.id})`, inline:true},
                    {name: "Removed by", value: message.author.tag, inline:true},
                    {name: "Reason", value: DenyReason[args[1] - 1]}
                );

            UserGuildLog.send(logExternal);
            User.send(verifiedDM);
            verificationGuild.channels.cache.get('768027252943880203').send(logInternal);

            /*fromGuild[args[0]] = {
                guildID: fromGuild[args[0]].guildID,
                verificationSent: false
            };
            fs.writeFile("./src/data/verification-guild.json", JSON.stringify(fromGuild), (err) => {
            if (err) console.log(err)});*/
        }
    }
}