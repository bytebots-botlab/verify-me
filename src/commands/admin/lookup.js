const {MessageEmbed} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "lookup",
    category: "admin",
    description: "Lookup info",
    run: async (client, message, args) => {
        message.delete() 

        if(!message.member.hasPermission(0x00000008)) return message.reply("You need permission `ADMINISTRATOR` to use command `%lookup`").then(m => m.delete({timeout: 5000}));

        if(!args[0]) return message.reply("You need to specify user by ID, `%lookup <userID>`").then(m => m.delete({timeout: 5000}));

        let fromGuild = JSON.parse(fs.readFileSync("./src/data/verification-guild.json", "utf8"))

        if(!fromGuild[args[0]]) return message.reply(`No records of \`${args[0]}\``)
        let UserGuild = client.guilds.cache.get(fromGuild[args[0]].guildID)
        const verificationGuild = client.guilds.cache.get('764248014033780817')
        let verifieduser = JSON.parse(fs.readFileSync("./src/data/verifieduser.json", "utf8"));
        let verifiedStatus = null
        let searchTarget = null

        if(verifieduser[args[0]]){
            verifiedStatus = `<:verifyme:768390443696128001> (verified by ${verifieduser[args[0]].verifiedBy})`
        } else {
            verifiedStatus = "Not verified yet"
        }

        if(message.guild.members.cache.get(args[0])){
            searchTarget = ":white_check_mark"
        } else {
            searchTarget = ":x:"
        }

        const embed = new MessageEmbed()
            .setTitle("Lookup Information")
            .setTimestamp()
            .setColor("#6fabe6")
            .setFooter("All user lookups are recorded.")
            .setThumbnail(UserGuild.members.cache.get(args[0]).user.displayAvatarURL())
            .setImage(fromGuild[args[0]].verificationImg)
            .addFields(
                {name: "User", value: UserGuild.members.cache.get(args[0]).user.tag, inline:true},
                {name: "User ID", value: UserGuild.members.cache.get(args[0]).user.id, inline:true},
                {name: "Verification status", value: verifiedStatus}
            );
        const warningMsg = new MessageEmbed()
            .setTitle("NEW LOOKUP")
            .setTimestamp()
            .setColor("#6fabe6")
            .setFooter(message.guild.name, message.guild.iconURL())
            .addFields(
                {name: "User", value: `${message.author.tag} (${message.member.id})`, inline:true},
                {name: "Server", value: `${message.guild.name} (${message.guild.id})`, inline:true},
                {name: "Target user", value: UserGuild.members.cache.get(args[0]).user.tag},
                {name: "Is target in server?", value: searchTarget}
            );

        message.reply("<:verifyme:768390443696128001> Check your DMs!").then(m => m.delete({timeout: 10000}));
        message.member.send(embed);
        verificationGuild.channels.cache.get('768416713187917824').send(warningMsg);
    }
}