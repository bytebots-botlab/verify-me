const {MessageEmbed} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "removalrequest",
    category: "admin",
    description: "Removal request",
    run: async (client, message, args) => {
        message.delete() 

        if(!message.member.hasPermission(0x00000020)) return message.reply("You need permission `MANAGE_SERVER` to use command `%removalrequest`").then(m => m.delete({timeout: 5000}));

        if(!args[0]) return message.reply("You need to specify user by ID, `%removalrequest <userID>`").then(m => m.delete({timeout: 5000}));

        const verificationGuild = client.guilds.cache.get('764248014033780817')
        let verifieduser = JSON.parse(fs.readFileSync("./src/data/verifieduser.json", "utf8"));

        if(!verifieduser[args[0]]) return message.reply(`\`${args[0]}\` is not verified!`).then(m => m.delete({timeout: 5000}));

        const embed = new MessageEmbed()
            .setTitle("Removal request")
            .setTimestamp()
            .setColor("#6fabe6")
            .setDescription("Please provide reason in **one message**, why this user's verification should be removed?");

        message.reply("<:verifyme:768390443696128001> Check your DMs!").then(m => m.delete({timeout: 10000}));
        message.member.send(embed)
        .then(msg => {
            let filter = m => !m.author.bot && m.channel.type === "dm";
            msg.channel.awaitMessages(filter, {max: 1})
        .then(info =>{
            const confirm = new MessageEmbed()
                .setTitle("Removal request recorded<:verifyme:768390443696128001>")
                .setTimestamp()
                .setColor("#6fabe6");
            message.member.send(confirm)


            const rrMsg = new MessageEmbed()
                .setTitle("REMOVAL REQUEST")
                .setTimestamp()
                .setColor("#FF5668")
                .addFields(
                    {name: "Requesting user", value: message.author.tag},
                    {name: "Target user ID", value: args[0]},
                    {name: "Reason for removal", value: info.first().content}
                );
            verificationGuild.channels.cache.get('768416713187917824').send(rrMsg);
        })})
    }
}