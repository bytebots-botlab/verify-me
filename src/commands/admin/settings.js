const {MessageEmbed} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "settings",
    category: "admin",
    description: "Settings stuff",
    run: async (client, message, args) => {
        message.delete()

        if(!message.member.hasPermission(0x00000020)) return message.reply("You need permission `MANAGE_SERVER` to use command `%settings`").then(m => m.delete({timeout: 5000}));

        if(!args[0]){
            const settingshelp = new MessageEmbed()
                .setTitle("Verify Me! - Settings")
                .setColor("#6fabe6")
                .setTimestamp()
                .setThumbnail(client.user.displayAvatarURL())
                .setFooter(`Settings help requested by ${message.author.tag}`, message.author.displayAvatarURL())
                .addFields(
                    {name: "<:verifyme:768390443696128001> Verified user role", value: "`%settings verifyrole <roleID/mention role>`\nChange verified user role."},
                    {name: ":triangular_flag_on_post: Prefix", value: "`%settings prefix <new prefix>`\nChange bot prefix."}
                );
            message.channel.send(settingshelp);
        }
        if(args[0] === "verifyrole"){
            if(!args[1]) return message.reply("Correct usage: `%settings verifyrole <roleID/mention role>`").then(m => m.delete({timeout: 5000}));

            const chgrole = message.mentions.roles.first() || message.guild.roles.cache.get(args[1]);

            if(!chgrole) return message.reply(`Can not find role \`${args[1]}\``);

            let guildrole = JSON.parse(fs.readFileSync("./src/data/guildroles.json", "utf8"));
            guildrole[message.guild.id] = {
                role: chgrole.id
            };
            fs.writeFile("./src/data/guildroles.json", JSON.stringify(guildrole), (err) => {
                if (err) console.log(err)
            });

            const done = new MessageEmbed()
                .setTitle("<a:approved:765040824941281311>:gear: Setting: Verified role changed")
                .setColor("#86FF56")
                .setTimestamp()
                .setFooter(message.author.tag, message.author.displayAvatarURL())
                .setDescription(`${message.author.tag} changed server's verified role successfully to \`${chgrole.name}\``);
            message.channel.send(done)
        }
        if(args[0] === "prefix"){
            if(!args[1]) return message.reply("Correct usage: `%settings prefix <new prefix>`").then(m => m.delete({timeout: 5000}));
            let prefixes = JSON.parse(fs.readFileSync("./src/data/prefixes.json", "utf8"));
            prefixes[message.guild.id] = {
                prefixes: args[1]
            };
            fs.writeFile("./src/data/prefixes.json", JSON.stringify(prefixes), (err) => {
                if (err) console.log(err)
            });

            const embed = new MessageEmbed()
                .setTitle("<a:approved:765040824941281311>:gear: Settings: Prefix changed")
                .setDescription(`Prefix changed to \`${args[1]}\``)
                .setTimestamp()
                .setColor("#86FF56")
                .setFooter(message.author.tag, message.author.displayAvatarURL());

            message.channel.send(embed);
        }
    }
}