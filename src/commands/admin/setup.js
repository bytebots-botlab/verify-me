const {MessageEmbed} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "setup",
    category: "admin",
    description: "Setup server",
    run: async (client, message, args) => {
        message.delete()

        if(!message.member.hasPermission(0x00000008)) return message.channel.send("You need `ADMINISTRATOR` permission to use this command.").then(msg => msg.delete({timeout: 5000}))

        if(!message.guild.me.hasPermission(0x00000010)) return message.channel.send("Hey, I am missing critical permission `MANAGE_CHANNELS`, please give me this permission so I can proceed.")
        
        let guildsetup = JSON.parse(fs.readFileSync("./src/data/guilds.json", "utf8"));
        if(!guildsetup[message.guild.id]){
            message.member.guild.channels.create('Verify Me!', {
                type: 'category',
            }).then(cg => {
                const everyone = message.guild.roles.cache.find(role => role.name === "@everyone");
                message.member.guild.channels.create('verifications', {
                    type: 'text'
                }).then(ch => {
                    ch.setParent(cg.id);
                    guildsetup[message.guild.id] = {
                        log: ch.id
                    };
                    fs.writeFile("./src/data/guilds.json", JSON.stringify(guildsetup), (err) => {
                        if (err) console.log(err)
                    });
                    ch.overwritePermissions([
                        {
                            id: everyone.id,
                            deny: ['VIEW_CHANNEL'],
                        },
                    ], 'Removing view access from everyone.')
                });
            });

            message.guild.roles.create({
                data: {
                    name: 'Verified Member',
                },
                reason: 'Created verification role for guild'
            }).then(role => {
                let guildrole = JSON.parse(fs.readFileSync("./src/data/guildroles.json", "utf8"));
                guildrole[message.guild.id] = {
                    role: role.id
                };
                fs.writeFile("./src/data/guilroles.json", JSON.stringify(guildrole), (err) => {
                    if (err) console.log(err)
                });
            });

            const embed = new MessageEmbed()
                .setTimestamp()
                .setColor("#336F3B")
                .setTitle("Setup is done <a:approved:765040824941281311>")
                .setDescription("Channel has been created. Now when your members complete verifications and our official verifiers approve the verification, you will get notification there.\n\nNow to setup verification info for your members, go to any channel you want and use command `%info`\n\nIf you want to use your own verified user role, use command `%settings verifyrole <roleID/mention role>`");

            message.channel.send(embed);
        } else {
            let waitMsg = await message.channel.send(`Seems like you have log created already, one moment.. resolving!`);
            let guildrole = JSON.parse(fs.readFileSync("./src/data/guildroles.json", "utf8"));

            if(message.guild.channels.cache.some(ch => ch.name === "verifications")){
                message.guild.channels.cache.find(ch => ch.name === "verifications").delete('Deleting old channel.')
            }
            if(message.guild.channels.cache.some(ch => ch.name === "Verify Me!")){
                message.guild.channels.cache.find(ch => ch.name === "Verify Me!").delete('Deleting old category.')
            }
            if(guildrole[message.guild.id]){
                if(message.guild.roles.cache.some(role => role.id === guildrole[message.guild.id].role)){
                    message.guild.roles.cache.find(role => role.id === guildrole[message.guild.id].role).delete('Deleting old role.')
                }
            }

            message.member.guild.channels.create('Verify Me!', {
                type: 'category',
            }).then(cg => {
                const everyone = message.guild.roles.cache.find(role => role.name === "@everyone");
                message.member.guild.channels.create('verifications', {
                    type: 'text'
                }).then(ch => {
                    ch.setParent(cg.id);
                    guildsetup[message.guild.id] = {
                        log: ch.id
                    };
                    fs.writeFile("./src/data/guilds.json", JSON.stringify(guildsetup), (err) => {
                        if (err) console.log(err)
                    });
                    ch.overwritePermissions([
                        {
                            id: everyone.id,
                            deny: ['VIEW_CHANNEL'],
                        },
                    ], 'Removing view access from everyone.')
                });
            });

            message.guild.roles.create({
                data: {
                    name: 'Verified Member',
                },
                reason: 'Created verification role for guild'
            }).then(role => {
                let guildrole = JSON.parse(fs.readFileSync("./src/data/guildroles.json", "utf8"));
                guildrole[message.guild.id] = {
                    role: role.id
                };
                fs.writeFile("./src/data/guildroles.json", JSON.stringify(guildrole), (err) => {
                    if (err) console.log(err)
                });
            });

            const embed = new MessageEmbed()
                .setTimestamp()
                .setColor("#336F3B")
                .setTitle("Setup is done <a:approved:765040824941281311>")
                .setDescription("Channel has been created. Now when your members complete verifications and our official verifiers approve the verification, you will get notification there.\n\nNow to setup verification info for your members, go to any channel you want and use command `%info`");

            waitMsg.edit(null, embed)

        }
    }
}