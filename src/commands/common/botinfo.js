const {MessageEmbed} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "botinfo",
    category: "common",
    description: "info about the bot",
    run: async (client, message, args) => {
        message.delete()

        let totalSeconds = (client.uptime / 1000);
        let days = Math.floor(totalSeconds / 86400);
        totalSeconds %= 86400;
        let hours = Math.floor(totalSeconds / 3600);
        totalSeconds %= 3600;
        let minutes = Math.floor(totalSeconds / 60);

        const embed = new MessageEmbed()
            .setTitle("Verify Me! - Bot info")
            .setColor("#6fabe6")
            .setTimestamp()
            .setFooter(`Botinfo requested by ${message.author.tag}`, message.author.displayAvatarURL())
            .setThumbnail(client.user.displayAvatarURL())
            .setDescription("**Verify Me!** is age verification bot that verifies your server users age. This is handy bot to use in servers that are age restricted or full of NSFW content. Verifications will follow the user to every server that the **Verify Me!** is in. Meaning that they only need to verify once using the bot. Verifications are done through ByteBots Verification Team. All team members must follow confidentiality obligation because your privacy is the most important thing for us!")
            .addFields(
                {name: "Server count", value: `\`${client.guilds.cache.size}\``, inline: true},
                {name: "Servers total user count", value: `\`${client.users.cache.size}\``, inline: true},
                {name: "Bot owner", value: "Trixxie#0001", inline: true},
                {name: "Support server", value: "[Click here](https://discord.gg/3fCGPjz)", inline: true},
                {name: "Bot invite", value: "[Click here](https://discord.com/api/oauth2/authorize?client_id=767742019576791101&permissions=268954736&scope=bot)", inline: true},
                {name: "Current uptime", value: `${days} days, ${hours} hours, ${minutes} minutes`}
            )
        message.channel.send(embed);
    }
}