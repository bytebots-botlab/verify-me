const {MessageEmbed} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "help",
    category: "common",
    description: "help",
    run: async (client, message, args) => {
        message.delete()

        const embed = new MessageEmbed()
            .setTitle("Verify Me! - Help")
            .setColor("#6fabe6")
            .setTimestamp()
            .setFooter(`Help requested by ${message.author.tag}`, message.author.displayAvatarURL())
            .setThumbnail(client.user.displayAvatarURL())
            .addFields(
                {name: "Setup server", value: "`%setup`\nSet up server to be ready for Verify Me to operate."},
                {name: "Bot settings", value: "`%settings`\nDifferent settings you can change"},
                {name: "How to verify info", value: "`%info`\nShows little trivia, how to verify with the bot."},
                {name: "Ping Pong", value: "`%ping`\nCheck server latency"},
                {name: "Verify", value: "`%verifyme`\nStarts the verification process"},
                {name: "Report issues", value: "`%report <issue here>`\nSends bug report or feedback straight to our support server"},
                {name: "Bot info", value: "`%botinfo`\nShows some information about the bot"},
                {name: "Lookup", value: "`%lookup <userID>`\nAllows server administrator to check users verification process."},
                {name: "Removal request", value: "`%removalrequest <userID>`\nIf you have strong feeling that one of the verified members has provided for example false info during their verification process, you can report it this way."}
            )
        message.channel.send(embed);
    }
}