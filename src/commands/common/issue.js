const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "report",
    category: "manage",
    description: "Verify member",
    run: async (client, message, args) => {
        message.delete()

        if(!args[0]) return message.reply("Please provide reason to your report\n`%report <issue>`").then(m => m.delete({timeout: 5000}))
        const verificationGuild = client.guilds.cache.get('764248014033780817')

        const embed = new MessageEmbed()
            .setTitle("New report")
            .setTimestamp()
            .setColor("BLUE")
            .setFooter(message.guild.name, message.guild.iconURL())
            .setThumbnail(message.author.displayAvatarURL())
            .addFields(
                {name: "From user", value: `${message.author.tag} (${message.member.id})`, inline:true},
                {name: "From server", value: message.guild.name, inline:true},
                {name: "Report content", value: args.slice(0).join(" ")}
            );
        
        const embed2 = new MessageEmbed()
            .setTimestamp()
            .setTitle("Your report has been sent to our support server successfully")
            .setColor("GREEN")
            .setFooter(`Report sent by ${message.author.tag}`)
            .addField("Support server", "[Click here to join](https://discord.gg/3fCGPjz)");

            verificationGuild.channels.cache.get('768416713187917824').send(embed);
            message.channel.send(embed2);
    }
}