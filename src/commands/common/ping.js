const Discord = require("discord.js");

module.exports = {
    name: "ping",
    category: "common",
    description: "PING PONG",
    run: async (client, message, args) => {

        message.delete()

        let botMsg = await message.channel.send(
            `Pinging...`,
        );
        let ping = botMsg.createdTimestamp - message.createdTimestamp;
        let colorVar;
        let emojiVar;
        switch (true) {
            case ping < 150:
                colorVar = '#43b581';
                emojiVar = '<:green:765041120542588988>';
                break;
            case ping < 250:
                colorVar = '#43b581';
                emojiVar = '<:green:765041120542588988>';
                break;
            case ping < 350:
                colorVar = '#faa61a';
                emojiVar = '<:yellow:765041205289025577>';
                break;
            case ping < 400:
                colorVar = '#f04747';
                emojiVar = '<:red:765041183696748598>';
                break;
            default:
                colorVar = '#f04747';
                emojiVar = '<:red:765041183696748598>';
                break;
        }
        const embed = new Discord.MessageEmbed()
            .setColor(colorVar)
            .setTitle(`Ping!`)
            .setDescription(`<:verifyme:768390443696128001>${emojiVar} Pong ${ping}ms!`)
            .setTimestamp()
            .setFooter(message.author.tag, message.author.displayAvatarURL());
        botMsg.edit(null, embed);

    }
}