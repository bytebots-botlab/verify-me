const {MessageEmbed, WebhookClient} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "verifyme",
    category: "common",
    description: "Verify",
    run: async (client, message, args) => {
        message.delete()

        let fromGuild = JSON.parse(fs.readFileSync("./src/data/verification-guild.json", "utf8"))
        let verifieduser = JSON.parse(fs.readFileSync("./src/data/verifieduser.json", "utf8"));
        if(verifieduser[message.member.id]){
            const alreadyverified = new MessageEmbed()
                .setTitle("You are already verified <:verifyme:768390443696128001> :thumbsup:")
                .setDescription("You should not need to verify anymore with us!")
                .setColor("#6fabe6")
                .setTimestamp()

            return message.member.send(alreadyverified)
        }
        if(fromGuild[message.member.id]){
            if(fromGuild[message.member.id].verificationSent === true){
                const waitingverification = new MessageEmbed()
                    .setTitle("Be patient!! :alarm_clock:")
                    .setDescription("You've sent verification details to us already, please wait patiently that you will be approved!")
                    .setColor("#6fabe6")
                    .setTimestamp()

                return message.member.send(waitingverification)
            }
        }

        let guildsetup = JSON.parse(fs.readFileSync("./src/data/guilds.json", "utf8"));
        if(!guildsetup[message.guild.id]){

            const nosetup = new MessageEmbed()
                .setTitle("Server not set up! <a:denied:765040892482158593>")
                .setColor("#f04947")
                .setDescription("Seems like this server has not been set up yet. Please contact **Server Adminstrator** and ask them to set this server up with command `%setup`")
            
            message.member.send(nosetup);
        } else {
            const info = new MessageEmbed()
                .setTitle("Verification Instructions")
                .setTimestamp()
                .setFooter(message.guild.name, message.guild.iconURL())
                .setThumbnail(client.user.displayAvatarURL())
                .setImage("https://i.imgur.com/HNaD5dR.png")
                .setColor("#6fabe6")
                .addFields(
                    {name: "What now?", value: ":arrow_right: Send a photo of you holding your Identification Card and a paper with the date today and phrase `Verify Me!` in it. Make sure your ID and paper is clear and readable"},
                    {name: "Good to know", value: "Verifications may take up to 24 hours if there are heavy queue of verifications requests. You will also be notified if your verification is approved or denied. You may blur or strikeout any personal information from your ID **EXCEPT** date of birth."},
                    {name: "Privacy Policy", value: "Your privacy is our priority, please check our privacy terms of service *added soon!!!*"}
                );
            message.member.send(info)
            .then(dmMSG => {
                let filter = m => !m.author.bot && m.attachments.size > 0 && m.channel.type === "dm";
                dmMSG.channel.awaitMessages(filter, {max: 1})
                .then(ID1 => {
                    const sendinfo = new MessageEmbed()
                        .setDescription(":thumbsup: Great! Your information has been now sent to our verification team. Please wait patiently.")
                        .setTimestamp();
                    message.member.send(sendinfo)
                    const verifythis = new MessageEmbed()
                        .setTitle("New verification request")
                        .setColor("#46478d")
                        .setThumbnail(message.author.displayAvatarURL())
                        .setFooter(message.guild.name, message.guild.iconURL())
                        .setTimestamp()
                        .setImage(ID1.first().attachments.first().url)
                        .addFields(
                            {name: "Member", value: message.author.tag, inline: true},
                            {name: "Member ID", value: message.author.id, inline: true},
                            {name: "Verifying for", value: message.guild.name}
                        );
                    const verificationGuild = client.guilds.cache.get('764248014033780817')
                    verificationGuild.channels.cache.get('767791223334240257').send("<@&767744783971778591>")
                    verificationGuild.channels.cache.get('767791223334240257').send(verifythis)

                    fromGuild[message.member.id] = {
                        guildID: message.guild.id,
                        verificationSent: true,
                        verificationImg: ID1.first().attachments.first().url
                    };
                    fs.writeFile("./src/data/verification-guild.json", JSON.stringify(fromGuild), (err) => {
                        if (err) console.log(err)
                    });

                    //verificationGuild.channels.cache.get('767791223334240257').send({files:[ID1.first().attachments.first().url]})
                })
            })
        }
    }
}