const {MessageEmbed} = require('discord.js');
const fs = require("fs");
module.exports = {
    name: "info",
    category: "common",
    description: "Information, how to verify",
    run: async (client, message, args) => {

        message.delete()

        //if(!message.member.hasPermission(0x00000020)) return message.channel.send("You need `MANAGE_SERVER` permission to use this command.").then(msg => msg.delete({timeout: 5000}))

        let guildsetup = JSON.parse(fs.readFileSync("./src/data/guilds.json", "utf8"));
        if(!guildsetup[message.guild.id]){

            const nosetup = new MessageEmbed()
                .setTitle("Server not set up! <a:denied:765040892482158593>")
                .setColor("#f04947")
                .setDescription("Seems like this server has not been set up yet. Please contact **Server Adminstrator** and ask them to set this server up with command `%setup`")
            
            message.member.send(nosetup);
        } else {
            const info = new MessageEmbed()
                .setTitle("Verification Information")
                .setTimestamp()
                .setFooter(message.guild.name, message.guild.iconURL())
                .setThumbnail(client.user.displayAvatarURL())
                .setColor("#6fabe6")
                .setDescription("Need to be verified? Well no problem, you can now verify yourself through ME and the best part is that once you are verified through **Verify Me!** you no longer need to verify again in other servers that use **Verify Me!** bot.")
                .addFields(
                    {name: "Who handles verifications?", value: "Verifications are handled by **ByteBots Official Verification Team**, to see these people or contact them you can join our server [here](https://discord.gg/3fCGPjz)"},
                    {name: "How to verify?", value: "Just use command `%verifyme` in any channel that you have write access and I will then DM you instructions."}
                );
            message.channel.send(info)
        }
    }
}