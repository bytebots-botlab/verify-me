const Discord = require("discord.js");

module.exports = {
    name: "servers",
    category: "dev",
    description: "Get all servers",
    run: async (client, message, args) => {

        message.delete()

        if(message.author.id === "102756256556519424"){
            if(!args[0]) return message.channel.send(client.guilds.cache.map(m => m.name).join('\n'));

            if(args[0] === 'id') return message.channel.send(client.guilds.cache.map(m => m.id).join('\n'));

            if(args[0] === 'leave'){
                if(!args[1]) return message.reply("What guild?").then(m => m.delete({timeout: 5000}))
                if(client.guilds.cache.find(g => g.id === args[1])){
                    let leaveguild = client.guilds.cache.get(args[1])
                    leaveguild.leave()

                    message.channel.send(`Successfully left guild **${leaveguild.name}**`);
                }
            }
               
        }
    }
}